"""Concurrency"""

# %%

from urllib.request import urlopen, HTTPError
from time import monotonic
from concurrent.futures import ThreadPoolExecutor

def url_time(url):
    start = monotonic()
    err = None

    try:
        with urlopen(url) as resp:
            resp.read()
    except HTTPError as e:
        err = e

    duration = monotonic() - start
    print(f'{url}: {duration:.3f}sec (error={err})')


urls = [
    'https://www.python.org/',
    'https://docs.python.org/3/',
    'https://cyesec.com/',
]

# Fan-out pattern
start = monotonic()
with ThreadPoolExecutor() as pool:
    for url in urls:
        pool.submit(url_time, url)
duration = monotonic() - start
print(f'total_time: {duration:.3f}sec')


# %%
from time import sleep

def add(a, b):
    print('add thread:', current_thread().ident)
    sleep(10)  # Simulate work
    return a + b

pool = ThreadPoolExecutor()
fut = pool.submit(add, 1, 2)
print(fut)

# %%
# Getting results
from threading import current_thread

computations = (
    (1, 2),
    (3, 4),
    (5, 6),
)

print('top thread:', current_thread().ident)

def callback(fut):
    print('callback thread:', current_thread().ident)
    print('cb:', fut.result())


futs = []
with ThreadPoolExecutor() as pool:
    for args in computations:
        fut = pool.submit(add, *args)
        futs.append(fut)
        fut.add_done_callback(callback)


for args, fut in zip(computations, futs):
    # print(fut.result())
    print(f'{args}: {fut.result()}')

# %%

def url_time(url):
    start = monotonic()

    try:
        with urlopen(url) as resp:
            resp.read()
    except HTTPError as e:
        pass

    return monotonic() - start


urls = [
    'https://www.python.org/',
    'https://docs.python.org/3/',
    'https://cyesec.com/',
]

# Fan-out pattern
start = monotonic()
with ThreadPoolExecutor() as pool:
    for url in urls:
        fut = pool.submit(url_time, url)

        # url=url to avoid closure bug
        def callback(fut, url=url):
            duration = fut.result()
            print(f'{url}: {duration:.3f}sec')

        fut.add_done_callback(callback)




# %%

def div(a, b):
    return a / b


futs = []
with ThreadPoolExecutor() as pool:
    fut = pool.submit(div, 1, 2)

print(fut.exception())
# fut.result() will raise if there's any exception


# %%

needle = 'ok'
haystack = 'this is cool'
if haystack.find(needle):
    print('found')
else:
    print('not found')










# %%


colors = (
    'red',
    'green',
    'blue',
)
print(colors)













