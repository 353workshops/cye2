# %%
class Object: ...
# o = object()  # Won't work, can't add attributes
o = Object()
o.user = 'joe'
print(o)  # default __str__  & __repr__
# %%

from types import SimpleNamespace

o = SimpleNamespace()
o.user = 'joe'
print(o)

# See also unittest.mock.Mock, unittest.mock.MagicMock

# %%

d = {'login': 'joe', 'uid': 7}
print(d)

d = {'login': 'joe', 'uid': '7'}
print(d)

from typing import TypedDict

# Use a schema for your messages with mypy
class User(TypedDict):
    login: str
    uid: int

u: User = {'login': 'joe', 'uid': '7'}  # mypy will fail

# Bruce Eckel: Strong Typing vs Strong Testing

# %% queue (FIFO)
# Use deque for FIFO
from collections import deque

# tasks = []
tasks = deque()
tasks.append('task 1')
tasks.append('task 2')
while tasks:
    # task = tasks.pop(0)
    task = tasks.popleft()
    print(task)
    

# %% performance
queue = [1] * 10_000

# List performance
%timeit queue.insert(0, 1) or queue.pop(0)


dq = deque(queue)
%timeit dq.appendleft(1) or dq.popleft()

# %%

d = deque([1,2,3])
d.rotate(1)
print(d)

# %%
d = deque(maxlen=3)
for i in range(10):
    d.append(i)
print(d)

# %%
def tail(it, n):
    return deque(it, maxlen=n)


print(tail(range(1000), 3))


# %%
poem = '''
The Road goes ever on and on
Down from the door where it began.
Now far ahead the Road has gone,
And I must follow, if I can,
Pursuing it with eager feet,
Until it joins some larger way
Where many paths and errands meet.
And whither then? I cannot say.
'''

# Print the most common word in the poem, case insensitive
# Word: one or more English letters ([a-zA-Z]+)

import re
from collections import defaultdict

# frequency = {}
frequency = defaultdict(int)
for word in re.findall(r'[A-Za-z]+', poem):
    word = word.lower()
    # frequency[word] = frequency.get(word, 0) + 1
    frequency[word] += 1

# print(frequency)
print(max(frequency, key=frequency.get))


# %%

from collections import Counter

# frequency = {}
frequency = Counter()
# 'hello there' -> ['hello', 'there']
# See also re.VERBOSE
for word in re.findall(r'[A-Za-z]+', poem):
    word = word.lower()
    frequency[word] += 1

# print(frequency)
# print(max(frequency, key=frequency.get))
print(frequency.most_common(1)[0][0])

# Can't -> can not
# Mr. -> mister
# 1.23 -> 1.23

# %%
frequency = Counter(
    w.lower()
    for w in re.findall(r'[A-Za-z]+', poem)
)
print(frequency.most_common(1)[0][0])


# %%
report = r'C:\to\new\report.csv'
print(report)


regexp = r'\d+'
print(regexp)

# r'\d+' is a "raw" string, \ is just a \

# %%
split_words = re.compile(r'[A-Za-z]+').findall
words = split_words(poem)
print(words[:10])



# %% Priority queue

# list: insert + sort: O(n*logn)
# heap: O(log n)

from heapq import heapify, heappop, heappush
from dataclasses import dataclass

@dataclass
class Task:
    priority: int
    task: str

    def __lt__(self, other):
        return self.priority < other.priority

tasks = [
    (7, 'coffee'),
    (3, 'exercise'),
#    (3, ('mow', 'lawn')),
    (0, 'wake up'),
    (4, 'feed cat'),
]
heapify(tasks)
heappush(tasks, (5, 'eat'))
print(tasks)
while tasks:
    task = heappop(tasks)
    print(task)

# tasks[0] is always top priority (lowest number)



# %% 

from bisect import bisect

values = [1, 3, 5, 7, 9]
n = 4
i = bisect(values, n)  # O(log n)
values.insert(i, n)
print(values)


# %%

from dataclasses import dataclass
import json

@dataclass
class Person:
    name: str
    planet: str

people = [
    Person('Ed', 'Earth'),
    Person('Spike', 'Mars'),
    Person('Fey', 'Earth'),
]

by_planet = defaultdict(set)
# by_planet = defaultdict(list)
for p in people:
    # by_planet[p.planet].append(p.name)
    by_planet[p.planet].add(p.name)

print(by_planet)

data = json.dumps(by_planet, default=list)
print(data)