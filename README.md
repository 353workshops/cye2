# Advanced Python Workshop @ CYE

Miki Tebeka
📬 [miki@353solutions.com](mailto:miki@353solutions.com), 𝕏 [@tebeka](https://twitter.com/tebeka), 👨 [mikitebeka](https://www.linkedin.com/in/mikitebeka/), ✒️[blog](https://www.ardanlabs.com/blog/)

[Syllabus](https://docs.google.com/document/d/17hRWq3wDUUmEAZ30h_5FCg8K_l7yuk32sSzvYCWVm5I/edit?usp=sharing)

#### Shameless Plugs

- [LinkedIn Learning Classes](https://www.linkedin.com/learning/instructors/miki-tebeka)
- [Books](https://pragprog.com/search/?q=miki+tebeka)

---

## Day 1: Programming Utilities

### Agenda

- String formatting in depth (directives, types, `__format__`)
- Advanced data structures & algorithms (collections, heap, bisect, SimpleNameSpace …)
- Iterables, iterators and generators
- Iteration utilities (zip, enumerate, reversed, sorted …)
- Python’s dark corners (scope, default values, floats, evaluation order …)
- Writing functions (design, *args, **kw, global & nonlocal, `__call__`, annotation, missing arguments, …)

### Code

- [funcs.py](funcs.py) - Defining and calling functions
- [dark.py](dark.py) - Dark corners of Python
- [iter.py](iter.py) - Iterables, iterators and generators
- [data_structures.py](data_structures.py) - More data structures (deque, defaultdict ...)
- [address.py](address.py) - String formatting

### Exercise

Write and ETL process from [logs.tar.gz](data/logs.tar.gz) to an sqlite3 database.
Each log file in `logs.tar.gz` is an XML file which contains XML records with the following fields:
- time
- level
- message

The database schema is:

```sql
-- schema.sql

CREATE TABLE logs (
    time TIMESTAMP,
    level VARCHAR(32),
    message TEXT
);

CREATE INDEX logs_time ON logs(time);
```

To create the database, you can run: `sqlite3 logs.db < schema.sql`

Make sure to load only a single record every time.
Use [xml.sax](https://docs.python.org/3/library/xml.sax.html) to parse XML.
Use [executemany](https://docs.python.org/3/library/sqlite3.html#sqlite3.Cursor.executemany) to insert records in a transaction.


### Links

- [pythontutor](https://pythontutor.com/) - Visualize Python execution
- [The Architecture of Open Source Applications](https://aosabook.org/en/) - Good reading
- Linters
    - [ruff](https://docs.astral.sh/ruff/)
    - [flake8](https://flake8.pycqa.org/) + [flake8-bugbear](https://github.com/PyCQA/flake8-bugbear)
    - [bandit](https://bandit.readthedocs.io/) - Security
- [itertools module](https://docs.python.org/3/library/itertools.html#itertools.count)
    - Read and understand the "Itertools Recipes" section
- [The only valid measurement of code quality](https://www.reddit.com/r/ProgrammerHumor/comments/1f9df7/the_only_valid_measurement_of_code_quality/)
- [Knuth vs McIlroy](https://matt-rickard.com/instinct-and-culture) - Power of command line tools
- [http.cat](https://http.cat/) - HTTP status with cats
- [mypy](https://mypy-lang.org/) - Static type checker
- [When Should You Use `__repr__` vs `__str__` in Python?](https://realpython.com/python-repr-vs-str/)
- [collections Module](https://docs.python.org/3/library/collections.html)
- [dataclasses Module](https://docs.python.org/3/library/dataclasses.html)
- [bisect module](https://docs.python.org/3/library/bisect.html)
- Iterators
    - [Iterators](https://docs.python.org/3/tutorial/classes.html#iterators) in the Python tutorial
    - [itertools](https://docs.python.org/3/library/itertools.html) - Iterator utilities (good reading ☺)
    - [Generator Tricks for System Programmers](http://www.dabeaz.com/generators/)
    - [Generators: The Final Frontier](https://www.youtube.com/watch?v=D1twn9kLmYg)


### Data & Other

- [http.log.gz](data/http.log.gz)
- [road.txt](data/road.txt)

---

## Day 2: Dependencies, Advanced OO, Optimization & Testing

### Agenda

- Dependency management (pip + virtualenv)
- Advanced OO (`__missing__`, `__getattr__`, MixIn & ABC, properties, descriptors)
- Creating and configuring objects (dependency injection, when to configure …)
- Context managers (`with` statement)
- Decorators
- Testing with pytest & hypothesis

### Code

- Testing
    - [test_nlp.py](nlp/tests/test_nlp.py) - Tests with pytest
    - [nlp.py](nlp/nlp.py) - Tested code
- [ctx_mgr.py](ctx_mgr.py) - Writing context managers
- [fields.py](fields.py) - Writing descriptors
- [decorators.py](decorators.py)
- [trade.py](trade.py) - Properties
- [plugin.py](plugin.py) - Abstract base classes (`ABC`) and `typing.Protocol`
- [missing.py](missing.py) - `__missing__` and `__getattr__`
- [imports.py](imports.py) - Import in Python

### Exercises

#### ORM

Write an ORM system. A user should be able to do:

```python
from datetime import datetime, UTC
import sqlite3

class Users(Record):
    id = Field('INTEGER')
    login = Field('TEXT')
    created = Field('TIMESTAMP')


db = sqlite3.connect('users.db')
u = User(7, 'bond', datetime(1953, 4, 13, tzinfo=UTC))
with db.cursor() as cur:
    u.save(db)
    u2 = User.from_db(7, cur)
```

#### Cache Decorator

Change the `cache` decorator to accept maximal size.
Implement `LRU` eviction policy.

### Links

- [Netflix Prize](https://en.wikipedia.org/wiki/Netflix_Prize)
- [Python Environment](https://xkcd.com/1987/) - XKCD
- [Falsehoods programmers believe about time zones](https://www.zainrizvi.io/blog/falsehoods-programmers-believe-about-time-zones/)
- [Falsehoods programmers believe about time](https://infiniteundo.com/post/25326999628/falsehoods-programmers-believe-about-time)
- [Year 2038 Problem](https://en.wikipedia.org/wiki/Year_2038_problem) - Time overflow on 32bit
- Package Management
    - [Miki's Guide](data/dependencies.md)
    - [Our Software Dependency Problem](https://research.swtch.com/deps)
    - [pip](https://pip.pypa.io/en/stable/)
    - [poetry](https://python-poetry.org/)
    - [pipenv](https://pipenv.pypa.io/en/latest/)
    - [pdm](https://pdm-project.org/en/latest/)
    - [pyenv](https://github.com/pyenv/pyenv)
    - [pyproject.toml](https://pip.pypa.io/en/stable/reference/build-system/pyproject-toml/)
    - [conda](https://docs.conda.io/en/latest/)
	- [Creating an environment](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#creating-an-environment-from-an-environment-yml-file)
    - [poetry](https://python-poetry.org/)
    - [pipenv](https://pipenv.pypa.io/en/latest/)
    - [pyenv](https://github.com/pyenv/pyenv) - Multiple Python versions
- OO
    - [abc Module](https://docs.python.org/3/library/abc.html)
    - [Python's property(): Add Managed Attributes to Your Classes](https://realpython.com/python-property/)
    - [Descriptors Guide](https://docs.python.org/3/howto/descriptor.html)
- Context Managers
    - [Context managers and Python's `with` Statement](https://realpython.com/python-with-statement/)
    - [contextlib](https://docs.python.org/3/library/contextlib.html)
- Decorators
    - [Primer on Python Decorators](https://realpython.com/primer-on-python-decorators/)
    - [functools.wraps](https://docs.python.org/3/library/functools.html#functools.wraps)
- Testing
    - [How SQLite is Tested?](https://www.sqlite.org/testing.html)
    - [pytest](https://docs.pytest.org/) - *The* test suite
    - [pytest-cov](https://pytest-cov.readthedocs.io/) - Coverage support to pytest
    - [hypothesis](https://hypothesis.readthedocs.io/) - Fuzz testing
    - [unittest.mock](https://docs.python.org/3/library/unittest.mock.html) - Mocking
    - [test containers](https://github.com/testcontainers/testcontainers-python)

### Data & Other

- [tokenize_cases.yml](data/tokenize_cases.yml)

---

## Day 3: Concurrency, Networking & More

### Agenda

- Speed & memory optimizations
- Concurrency with threads, processes & asyncio
- Networking
- A look at Python’s new features
    - Walrus operator, type hints, pattern matching …
- Effective serialization
    - JSON, protocol buffers, pickle and more
- Fun with Unicode
- Going faster with Cython


### Code

- [serialization.py](serialization.py) - Serialization, JSON & protobuf
- [new_features.py](new_features.py) - New Python features
- [socket_client.py](socket_client.py) - TCP socket client
- [socket_server.py](socket_server.py) - TCP socket server
- [check_links.py](check_links.py) - asyncio
- [jury.py](jury.py) - Common bugs in ProcessPoolExecutor & ThreadPoolExecutor
- [taxi_check.py](taxi_check.py) - Convert sequential algorithm to parallel
- [urls.py](urls.py) - Concurrency
- [nlp](nlp) - Testing & optimization

### Exercises

#### TCP Server

Write a TCP server that will handle each request in a new thread.
The server can receive one of the following messages:
- `{"type": "time"}`-> `{"time": "2024-05-22T17:36:23.654907+00:00"}`
- `{"type": "ping", "count": 3, "host": "apple.com"}` -> `{"host": "apple.com", "times": [66.8, 72.8, 67.4]}`

#### Download Size

Write a function `downloadSize(year)` that will return the total download size of files for that year from
[TLC Trip Record Data](https://www.nyc.gov/site/tlc/about/tlc-trip-record-data.page).

The data files can be either from `yellow` or `green`, example data file URL:
`https://d37ci6vzurychx.cloudfront.net/trip-data/yellow_tripdata_2023-03.parquet`

Use an HTTP `HEAD` request to get the size and use `asyncio` to make the requests concurrently.

### Links

- [Animal group names](https://arapahoelibraries.org/blogs/post/names-for-groups-of-animals/)
- [Error groups](https://www.youtube.com/watch?v=rKBKHVvXuUI&list=PLnOlTVPC-yFwhpiyjsC0V_98pqRbtwNgh&index=1&pp=iAQB)
- Performance Optimization
    - [The Python Profilers](https://docs.python.org/3/library/profile.html)
    - [Faster Python Code](https://www.linkedin.com/learning/faster-python-code/)
    - [py-spy](https://github.com/benfred/py-spy)
    - [Computer Latency at Human Scale](https://twitter.com/jordancurve/status/1108475342468120576/photo/1)
    - [snakeviz](https://jiffyclub.github.io/snakeviz/)
    - [line_profiler](https://github.com/pyutils/line_profiler)
    - [pytest-benchmark](https://pytest-benchmark.readthedocs.io/en/latest/)
    - [Time complexity](https://wiki.python.org/moin/TimeComplexity)
    - [Performance mantras](https://www.brendangregg.com/blog/2018-06-30/benchmarking-checklist.html)
    - [Rules of optimization club](https://wiki.c2.com/?RulesOfOptimizationClub)
- Concurrency
    - [concurrent.futures module](https://docs.python.org/3/library/concurrent.futures.html)
    - [threading module](https://docs.python.org/3/library/threading.html)
    - [multiprocessing module](https://docs.python.org/3/library/multiprocessing.html)
    - [C10k problem](https://en.wikipedia.org/wiki/C10k_problem)
    - [Amdhal's Law](https://en.wikipedia.org/wiki/Amdahl%27s_law)
- asyncio
    - [asyncio module](https://docs.python.org/3/library/asyncio.html)
    - [aiohttp](https://docs.aiohttp.org/en/stable/)
    - [httpx](https://www.python-httpx.org/)
    - [FastAPI](https://fastapi.tiangolo.com/)
- Networking
    - [socket module](https://docs.python.org/3/library/socket.html)
    - [socketserver module](https://docs.python.org/3/library/socketserver.html)
    - [select module](https://docs.python.org/3/library/select.html)
    - [Networking and Interprocess Communication](https://docs.python.org/3/library/ipc.html)
    - [Twisted framework](https://twisted.org/)
    - [ØMQ](https://zguide.zeromq.org)
- Serialization
    - [Overview](data/serialization.md)
    - [json module](https://docs.python.org/3/library/json.html)
    - [pickle module](https://docs.python.org/3/library/pickle.html)
    - [Protocol Buffers](https://protobuf.dev/)
        - [gRPC](https://grpc.io/)
        - [buf](https://buf.build/)
    - [Chunked transfer encoding](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Transfer-Encoding)
    - [XML billion laughs](https://en.wikipedia.org/wiki/Billion_laughs_attack)
- Unicode
    - [Unicode table](https://en.wikipedia.org/wiki/List_of_Unicode_characters)
    - [Unicode HOWTO](https://docs.python.org/3/howto/unicode.html)
    - [unicodedata module](https://docs.python.org/3/library/unicodedata.html)
    - [chardet](https://github.com/chardet/chardet) - Detect encoding
- [Cython](https://cython.readthedocs.io/en/latest/)


### Data & More

- [Layers](data/layers.png)
- `protoc --python_out=. trade.proto`
- `python setup_cython.py build_ext --inplace`
- `Σ`, `σ`, `ς`
- `'Kraków`, `Kraków`
