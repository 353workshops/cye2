from socket import socket
from time import sleep

sock = socket()
sock.connect(('localhost', 8080))
for i in range(3):
    message = f'message #{i}\n'
    sock.sendall(message.encode())
    response = sock.recv(1024)
    print(response.decode())
    sleep(0.5)