# %%
from socket import socket, SOL_SOCKET, SO_REUSEADDR

sock = socket()  # AF_INET, SOCK_STREAM
sock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1) # enable address reuse

# QUIC: UDP based, will be HTTP3

address = ('localhost', 8080)
# Use ('0.0.0.0', 8080) to listen on all interfaces
sock.bind(address)
sock.listen(10)

print(f'server starting on {address}')
while True:
    conn, addr = sock.accept()
    print(f'connection from {addr}')
    file = conn.makefile('r')
    for line in file:
        print(f'server: {line[:-1]}')
        conn.sendall(line.upper().encode())
    print('client disconnected')
