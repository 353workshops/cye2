"""Context Managers (with)"""

# %%

try:
    with open('ctx_mgr.py') as fp:
        print('inside:', fp.closed)
        1/0
except ZeroDivisionError:
    pass

print('outside:', fp.closed)

# %%

class CtxMgr:
    def __init__(self, name, masked=None):
        self.name = name
        self.masked = () if masked is None else masked
        print(f'{name}: __init__')

    def __enter__(self):
        print(f'{self.name}: __enter__')
        print('BEGIN')
        return self

    def __exit__(self, typ, val, tb):
        print(f'{self.name}: __exit__')
        if typ is None:
            print('COMMIT')
        else:
            print('ROLLBACK')
        
        # If __exit__ return truthy value
        # The exception is masked
        if isinstance(val, self.masked):
            return True


# try:
# except ValueError as err
#        typ           val


# with CtxMgr('first'):  # __init__
with CtxMgr('first', ZeroDivisionError) as first, CtxMgr('two'):  # __init__
    # __enter__
    print('IN:', first)
    1/0
    # ...
    # __exit__


# %%

class Closing:
    def __init__(self, obj):
        self.obj = obj

    def __enter__(self):
        return self.obj
    
    def __exit__(self, typ, val, tb):
        self.obj.close()


with Closing(open('ctx_mgr.py')) as fp:
    print(fp.readline())


# %%
import sqlite3

db = sqlite3.connect(':memory:')
db.executescript('''
CREATE TABLE logs (id INTEGER, message TEXT);
''')

sql = '''
INSERT INTO logs 
    (id, message)
VALUES 
    (?, ?)
'''

with closing(db.cursor()) as cur, cur:
    cur.execute(sql, (1, 'INFO: HI'))

# cur __exit__ will commit/rollback
# closing.__exit__ will close cur

# There also __del__ which is called when GC kicks in
# Don't use __del__


# %%
# Exercise:
# Write a "timed" context manager that prints time of
# operation.
# Timed between __enter__ and __exit__
from time import sleep, perf_counter

class timed:
    def __init__(self, name):
        self.name = name
    
    def __enter__(self):
        self.start = perf_counter()

    def __exit__(self, typ, val, tb):
        duration = perf_counter() - self.start
        print(f'{self.name} took {duration:.5f}sec')

with timed('sleep'):
    sleep(0.2)
# sleep took 0.2sec


# %% 
from contextlib import contextmanager
from time import perf_counter
from sys import exc_info

@contextmanager
def timed2(name):
    # __enter__
    start = perf_counter()

    try:
        yield  # user code
    finally:
        # __exit__
        duration = perf_counter() - start
        typ, val, tb = exc_info()
        if typ:
            print('error')
        print(f'{name} took {duration:.05f}sec')

with timed2('sleep2'):
    sleep(0.3)
    1/0


















