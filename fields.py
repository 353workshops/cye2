"""Descriptors

Work at class level
"""

# %%
class Field:
    attr_name = '_fields'

    def __get__(self, inst, owner):
        print(f'__get__: {inst=}, {owner=}')
        if inst is None:  # class access
            return self

        values = getattr(inst, self.attr_name, {})
        if self.key not in values:
            raise AttributeError(self.name)

        return values[self.key]

    def __set__(self, inst, value):
        print(f'__set__: {inst=}, {value=}')
        self.validate(value)

        values = getattr(inst, self.attr_name, {})
        values[self.key] = value
        setattr(inst, self.attr_name, values)
    
    def __set_name__(self, owner, name):
        cls_name = owner.__name__
        self.key = f'_{cls_name}_{name}'
        self.name = name

    def validate(self, value):
        pass


class Symbol(Field):
    def validate(self, value):
        if not isinstance(value, str):
            raise TypeError('field must be str, got {value!r}')

        if not value.strip():
            raise ValueError('empty symbol')



class Trade:
    symbol = Symbol()  # __set_name__(Trade, 'symbol')
    price = Field()

    def __init__(self, symbol, price):
        self.symbol = symbol
        self.price = price


t1 = Trade('BRK.A', 650_000.123)
print('class:', Trade.symbol)  # access at class level
print('inst: ', t1.symbol)  # access at instance level
print('attrs:', t1.__dict__)
# t1.symbol = ''  # Will raise

# Exercise: Change Field to use a single attribute
# which is a dictionary. And store values there.



# %%

class StaticMethod:
    def __init__(self, func):
        self.func = func

    def __get__(self, inst, owner):
        return self.func


class Math:
    #@staticmethod
    @StaticMethod
    def neg(n):
        return -n


m = Math()
print(m.neg(3))













