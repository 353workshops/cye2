from nlp import tokenize

text = 'There should be one-- and preferably only one --obvious way to do it.'

for _ in range(1_000_000):
    tokenize(text)


# Print to screen, sorted by cumulative time
# python -m cProfile -s cumtime perf_tokenize.py

# Save to file
# python -m cProfile -o tok.pprof perf_tokenizer.py
# python -m pstats tok.pprof
# python -m snakeviz tok.pprof