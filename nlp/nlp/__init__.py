"""NLP: Natural language processing tools for Python"""
# Top level API

import re


_stop_words = [
    'is',
    'on',
    'the',
]


def is_stop(word):
    """Return true is `word` is a stop word (e.g. 'is')."""
    return word in _stop_words


def stem(word):
    """Return stemmed version of word.
    
    >>> stem('working')
    'work'
    >>> stem('works')
    'work'
    >>> stem('worked')
    'work'
    """
    for suffix in ('ed', 'ing', 's'):
        if word.endswith(suffix):
            return word[:-len(suffix)]

    return word


def tokenize(text):
    """Return tokens in text.

    >>> tokenize("Who's on first?")
    ['who', 'first']
    """
    return [
        s
        for tok in re.findall(r'[A-Za-z]+', text)
        if (s := stem(tok.lower())) and not is_stop(s)
    ]