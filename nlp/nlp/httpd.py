from fastapi import FastAPI, Request, Response
from http import HTTPStatus

from . import tokenize

app = FastAPI()

class HealthError(Exception): ...

def check_health():
    # TODO: Check connection to database ...
    ... 

@app.get('/health')
def health_handler():
    try:
        check_health()
        return Response(content='OK', media_type='text/plain')
    except HealthError as err:
        return Response(
            content=f'error: {err}',
            media_type='text/html',
            status_code=HTTPStatus.INTERNAL_SERVER_ERROR,
        )


@app.post('/tokenize')
async def tokenize_handler(request: Request):
    text = await request.body()
    return {
        'tokens': tokenize(text.decode()),
    }


if __name__ == '__main__':
    from os import getenv

    import uvicorn

    port = int(getenv('PORT') or 8080)
    # TODO: Check for valid port

    try:
        check_health()
    except HealthError as err:
        raise SystemExit(f'error: {err}')

    uvicorn.run(app, port=port)
