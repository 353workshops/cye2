from urllib.request import urlopen
from http import HTTPStatus



def test_health(server):
    url = f'http://localhost:{server.port}/health'
    with urlopen(url) as resp:
        assert resp.status == HTTPStatus.OK
