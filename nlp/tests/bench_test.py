from nlp import tokenize

def test_tokenize_bench(benchmark):
    text = 'There should be one-- and preferably only one --obvious way to do it.'
    result = benchmark(tokenize, text)
    assert len(result) == 13
