from os import environ

import pytest
import yaml
from hypothesis import given
from hypothesis.strategies import text

import nlp


def test_tokenize():
    text = "What's on second?"
    expected = ['what', 'second']
    tokens = nlp.tokenize(text)
    assert expected == tokens


# tokenize_test_cases = [
#     # text, expected
#     ("Who's on first?", ['who', 's', 'first']),
#     ("What's on second?", ['what', 's', 'second']),
#     pytest.param('', [], id='empty'),
# ]

def load_tokenize_cases():
    with open('tests/tokenize_cases.yml') as fp:
        data = yaml.safe_load(fp)

    for case in data:
        args = (case['text'], case['tokens'])
        if 'name' in case:
            args = pytest.param(*args, id=case['name'])
        yield args

# @pytest.mark.parametrize('text, expected', tokenize_test_cases)
@pytest.mark.parametrize('text, expected', load_tokenize_cases())
def test_tokenize_many(text, expected):
    tokens = nlp.tokenize(text)
    assert expected == tokens


in_ci = 'CI' in environ

@pytest.mark.skipif(not in_ci, reason='not in CI')
def test_in_ci():
    ...


def test_error():
    with pytest.raises(ZeroDivisionError):
        1/0


@given(text())
def test_fuzz_tokenize(text):
    # print(text)
    tokens = nlp.tokenize(text)
    low_text = text.lower()
    for tok in tokens:
        assert tok in low_text
