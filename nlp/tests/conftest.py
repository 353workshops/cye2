from collections import namedtuple
from os import environ
from socket import socket
from subprocess import Popen
from sys import executable
from time import monotonic, sleep

import pytest


def free_port():
    with socket() as sock:
        # Using port 0 will allocate random free port
        sock.bind(('localhost', 0))
        return sock.getsockname()[1]


def wait_for_server(port, timeout):
    addr = ('localhost', port)

    start = monotonic()
    while monotonic() - start <= timeout:
        with socket() as sock:
            try:
                sock.connect(addr)
                return
            except OSError:
                sleep(0.1)

    assert False, f'{addr} not reachable after {timeout}'


Server = namedtuple('Server', 'port')

class Killing:
    def __init__(self, proc):
        self.proc = proc
    
    def __enter__(self):
        return self.proc

    def __exit__(self, typ, val, tb):
        self.proc.kill()


@pytest.fixture
def server():
    # Setup
    port = free_port()
    env = environ.copy()
    env['PORT'] = str(port)
    cmd = [executable, '-m', 'nlp.httpd']
    proc = Popen(cmd, env=env)

    wait_for_server(port, 3)

    # with Killing(proc):
    #     yield Server(port=port)
    yield Server(port=port)

    # Teardown
    proc.kill()
