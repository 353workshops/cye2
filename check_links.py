# %%
from urllib.request import urlopen, URLError
from http import HTTPStatus
from time import perf_counter
from aiohttp import ClientError, ClientSession
import asyncio


urls = [
    'https://cyesec.com',
    'https://httpbin.org/status/404',
    'https://no-such-domain.com',
]


async def check_link_async(url):
    async with ClientSession() as client:
        try:
            async with client.get(url) as resp:
                    if resp.status != HTTPStatus.OK:
                        print(f'{url} - {resp.status}')
                    else:
                        print(f'{url} - OK')
        except ClientError as err:
                print(f'{url} - {err}')


async def check_links_async(urls):
    await asyncio.gather(*[check_link_async(url) for url in urls])
    

start = perf_counter()
asyncio.run(check_links_async(urls))
print(perf_counter() - start)