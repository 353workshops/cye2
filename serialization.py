"""Serialization"""

# %%
from dataclasses import dataclass, asdict
from enum import Enum
import json
from datetime import datetime, UTC

class State(str, Enum):
    starting = 'STARTING'
    running = 'RUNNING'
    down = 'DOWN'


@dataclass
class VM:
    id: str
    state: State
    started: datetime
    tags: set[str]


def default(obj):
    if isinstance(obj, set):
        return list(obj)
    if isinstance(obj, datetime):
        return obj.isoformat()
    raise TypeError(obj)


vm = VM(
    '007', 
    'running', 
    datetime.now(UTC),
    {'MI6', 'London'},
)
data = json.dumps(asdict(vm), default=default)
print(data)


# For load/loads see object_hook or object_pairs_hook

# 4/1/2024
# 2024-04-01
# RFC3339, ISO68...

# %% streaming
# jsonlines

from io import StringIO

io = StringIO()

for i in range(3):
    json.dump({'id': i}, io)
    io.write('\n')



io.seek(0, 0)
for line in io:
    data = json.loads(line)
    print(data)

# See HTTP chunked transfer encoding


# %% protobuf

from trade_pb2 import Trade
from datetime import datetime
from google.protobuf.timestamp_pb2 import Timestamp

t = Trade(
    symbol='IBM',
    # time=datetime.now(),
    price=37.9,
    volume=19,
    is_buy=True,
)
# t.time.GetCurrentTime(),
t.time.FromDatetime(datetime.now())
print(t)


# %%

text = 'שלום'
print(text)
print(text[0])
print(len(text))
data = text.encode('utf-8')
print(len(data))

s = data.decode('utf-8')
print(s)

# encode going out
# decode coming in, use str internally

# %%

c1, c2, c3 = 'Σ', 'σ', 'ς'
print(c1.lower() == c2.lower())  # True
print(c1.lower() == c3.lower())  # False
print(c1.casefold() == c3.casefold())  # True

# %%
from unicodedata import normalize

city1, city2 = 'Kraków', 'Kraków'
print(city1 == city2)
print(len(city1), len(city2))

print(city1 == normalize('NFC', city2))
# When decoding, always normalize














