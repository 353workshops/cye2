"""Abstract Base Classes"""


# %%

from abc import ABC, abstractmethod

# class Plugin(ABC):
#     """Plugin is an interface"""
#     @abstractmethod
#     def setup(self, ui):
#         # Never executed, write whatever
#         raise NotImplementedError()

#     @abstractmethod
#     def execute(self):
#         raise NotImplementedError()


# class LoggingPlugin(Plugin):
#     def setup(self, ui):
#         print('logging: setup')

#     def execute(self):
#         print('logging: execute')


# %% With mypy & typing.Protocol

from typing import Protocol

class Plugin(Protocol):
    """Plugin is an interface"""
    def setup(self, ui: UI):
        ...

    def exectue(self):
        ...


class LoggingPlugin(Plugin):
    def setup(self, ui):
        print('logging: setup')

    def execute(self):
        print('logging: execute')



class UI:
    def __init__(self, plugins):
        self.plugins = plugins
        for p in plugins:
            p.setup(self)
    
    def on_change(self):
        for p in self.plugins:
            p.execute()

ui = UI([LoggingPlugin()])
ui.on_change()
