# Serialization

[TOC]

## Overview

Computers understand only bits and bytes.
Every type you work in Python is stored as a sequence of bytes in memory.

For example:[^pyobj]

| Value             | Type  | Encoding (big endian)              |
| ---               | ---   | ---                                |
| 1234567890        | int64 | 00000000499602d2                   |
| 3.141592653589793 | float | 400921fb54442d18                   |
| "«Serialization»" | str   | c2ab53657269616c697a6174696f6ec2bb |

[^pyobj]: Python's types in the C level are actually `PyObject *`.


You need to explicitly serialize at the edges of your program.
Your code should work with native types (for example `datetime` and not `'2024-05-12T13:45:12'`).

![](layers.png)

## Selecting Format

If you're lucky to be early enough in the project life cycle,
you can select the appropriate serialization format.
Here are some points you should consider:

- Maturity: Is the format mature? (see [Lindy effect](https://en.wikipedia.org/wiki/Lindy_effect)
- Adoption: How many languages support this format?
    - pickle: Python only
    - JOSN: Most languages
- Types: Does the format support the types you need. For example, JSON does not have a `TIMESTAMP` type.
- Schema: Does the format has a schema?
    - Protocol buffers have a schema
    - JSON don't have a schema
- Performance: How much time does it take to encode decode? How many bytes are generated?
    - From my experience Protocol buffers is about 3 times faster than JSON and generate ¼ of the bytes
- Queryability: Can you query the data directly? (for example: SQLite, parquet)
- Security: How secure is the format?
- Streaming: Is there built-in support for streaming of data?
- Standard library: Is the format in the standard library? If so it's one less third-party dependency you need to worry about.

## Formats Overview

- pickle: Python only, binary, can encode most types, no schema, built-in.
    - [dill](https://dill.readthedocs.io/) can encode more :)
- JSON: Wide adoption, textual, limited set of types, no schema, built-in.
- YAML & TOML: Wide adoption, textual, limited set of types, no schema, TOML built-in. Mostly used for configuration
- XML: Wide adoption, textual, no types, no schema (unless you use DTD or XML schema), built-in.
- CSV: Wide adoption, textual, no types, no schema, built-in. Don't use :)
- Protocol buffers: Decent adoption, binary, a lot of types, has schema, external.
- SQL: Wide adoption, binary, a lot of types, has schema, `sqlite3` built-in.

And may more: msgpack, flat buffers, capnproto, parquet, Avro, ORC, HDF5, Arrow, ...
