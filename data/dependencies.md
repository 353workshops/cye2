# Using pip and virtualenv

Python has more than 150,000 packages on [pypi][pypi],
the recommended way to install them is by using [pip][pip].

By default, `pip` installs packages to global Python.
This is not recommended since a lot of Linux systems use Python to manage the system, and you can break things.
The most common way to [venv][venv] which creates a local environment for installing packages.

[venv]: https://docs.python.org/3/library/venv.html

## Requirements and Versioning

By default, `pip` installs the latest version of a package.
However, it's highly recommended to control the exact version of the packages you use in your project.
You can do that by appending the version to the installed package.
e.g. `install elasticsearch==2.2.0`.

A common practice is to keep a `requirements.txt` file with a list of packages for the project,
and then install them with `pip install -r requirements.txt`.
You can view your current requirements by running `pip freeze`.

You should also separate the development requirements from the production ones.

### requirements.txt example

```
# Production requirements

PyYAML ~= 6.0
```

### dev-requirements.txt example

```
# Include production requirements
-r requirements.txt

pytest ~= 8.2
```

## Sample Flow

```bash
# Create virtual environment
$ python -m venv .venv

# Activate the virtual environment for the current shell
$ source .venv/bin/activate
$ which python
/path/to/project/.venv/bin/python

# Install requirements
$ python -m pip install -r dev-requirements.txt 
# ...

# Hack ...

# When done (optional), deactivate the virtual environment
$ deactivate 
```

## Other Package Managers

- [poetry](https://python-poetry.org/)
- [pipenv](https://pipenv.pypa.io/en/latest/)
- [pdm](https://pdm-project.org/en/latest/)
- [conda](https://conda.io/docs/)

## Notes

Installing "pure Python" packages is usually easy.
However, if the package you install requires some C libraries and headers it might be more challenging 
(for example when you install the PostgreSQL `psycopg2` driver, it'll look for PostgreSQL headers).
Be aware of that and try to find binary installs (called [wheels][wheel]).

Modern IDE's (such as [VSCode][vscode] and [PyCharm][pycharm] are aware) of virtual environments.
You need to set the current project interpreter to be the one from `venv/bin/python`.

[pycharm]: https://www.jetbrains.com/pycharm/
[vscode]: https://code.visualstudio.com/

When deploying code copy your project to a new machine and then create a virtual environment for it using the projects `requirements.txt` file

[install]: https://pip.pypa.io/en/stable/installing/
[pip]: https://pip.pypa.io/en/stable/
[pypi]: https://pypi.python.org/pypi
[venv]: https://virtualenv.pypa.io/en/latest/
[wheel]: https://bitbucket.org/pypa/wheel/
