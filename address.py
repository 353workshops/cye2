"""String formatting"""

# %%
class Address:
    def __init__(self, host, port):
        self.host = host
        self.port = port

    def __str__(self):
        return f'{self.host}:{self.port}'

    def __repr__(self):
        name = self.__class__.__name__
        return f'{name}({self.host!r}, {self.port!r})'

    def __format__(self, spec):
        # replace %h with host and %p with port
        spec = spec.replace('%h', self.host)
        spec = spec.replace('%p', str(self.port))
        return spec

class TCPAddr(Address): ...
    

addr = Address('localhost', 5432)

# print(addr.__dict__)
# print(vars(addr))
print('str :', addr)
print('repr:', repr(addr))
print(f'Connection to {addr:%h} on port {addr:%p}')

tcp = TCPAddr('localhost', 8080)
print(f'{tcp=}')

# %% str vs repr
# str is for users
# repr is for developers, usually a way to create such object

a, b = '1', 1
print(f'a={a}, b={b}')  # print uses __str__ by default
print(f'a={a!r}, b={b!r}')  # print uses __str__ by default
print(f'{a=}, {b=}') 

args = [repr(v) for v in (a, b)]
print(', '.join(args))

import logging
logging.error('a=%s, b=%s', a, b)  # Not recommended (not types)
logging.error('a=%r, b=%r', a, b)  # The recommended way
# Q: Why is this not recommended?
# A: Since the new string is always created regardless of the effective log level
logging.info(f'{a=}, {b=}')
# %% dataclass provides __repr__

from dataclasses import dataclass

@dataclass
class Location:
    lat: float
    lng: float

loc = Location(3.14, 2.718)
print(loc)

# %%
from pydantic import BaseModel

class Address(BaseModel):
    street: str
    number: str

addr = Address(street='fifth ave.', number='38')
print('str :', addr)
print('repr:', repr(addr))
# %%

from datetime import datetime

now = datetime.now()

print(f'Today is {now:%Y-%m-%d}')

# %%
