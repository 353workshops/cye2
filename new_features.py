"""Some of Python's newer features"""

# %% 3.9
defaults = {
    'port': 8080,
    'log_level': 'INFO',
} 

env = {
    'port': '9999',
}


# cfg = defaults.copy()
# cfg.update(env)
cfg = defaults | env
print(cfg)

# %%
from collections import Counter

events = ['click', ' ', 'move', '', 'click ']
# freq = Counter(e.strip() for e in events if e.strip())
freq = Counter(
    e.strip()
    for evt in events
    if (e := evt.strip())
)
# := walrus operator
print(freq)


# %% 3.8
import re
from io import StringIO

data = '''
2022-04-01 3.5
# comment
2022-04-02 8.5
'''
for line in StringIO(data):
    if (match := re.search(r'(\d{4}-\d{2}-\d{2}) (.*)', line)):
        print(match[1], '->', match[2])



# %% Extended unpacking

args = (1, 2, 3, 4)
a, *_, b = args  # unpacking
print(a, b)

tasks = ['wake', 'coffee', 'feed cat', 'eat']
while tasks:
    task, *tasks = tasks
    print(f'doing {task}')


k1, k2 = {'x': 1, 'y': 2}
print(k1, k2)







# %%
from dataclasses import dataclass

def user_from_db(uid: int) -> 'User':
    return User(id=uid)

u = user_from_db(7)
print(u)


@dataclass
class User:
    id: int
    


# %% statistics
import statistics

poem = '''
The Road goes ever on and on
Down from the door where it began
Now far ahead the Road has gone
And I must follow if I can
Pursuing it with eager feet
Until it joins some larger way
Where many paths and errands meet
And whither then I cannot say
'''
# By Bilbo Baggins

print(statistics.mode(w.lower() for w in poem.split()))

# %% basic types with type hints

def longest(names: list[str]) -> str:
    return max(names, key=len)

print(longest(['bilbo', 'frodo', 'sam']))

# %% 3.10

animal = ['fish', 'wolves', 'crows', 'frogs']
groups = ['school', 'pack', 'murder']
for a, g in zip(animal, groups, strict=True):
    print(f'I saw a {g} of {a}')

# %%

def handle(message):
    match message:
        case ['help']:
            print('Use the source Luke!')
        case ['vm-start', ami]:
            print(f'starting {ami}')
        case ['vm-start', count, ami]:
            print(f'starting {count} {ami}')

handle(['vm-start', 10, 'ubuntu'])


# %%

# MAX_X, MAX_Y = 400, 600
class Coords:
    MAX_X = 400
    MAX_Y = 600

def advance(coords):
    match coords:
        # case [MAX_X, MAX_Y]:  # BUG: Will always match
        # Option 1: guards
        # case [x, y] if (x, y) == (400, 600):
        case [Coords.MAX_X, Coords.MAX_Y]:
            return coords
        case [Coords.MAX_X, y]:
            return Coords.MAX_X, y + 1
        case [x, Coords.MAX_Y]:
            return x+1, Coords.MAX_Y
        case [x, y]:
            return x + 1, y + 1


print(advance([10, 20]))





























































