"""Decorators

Decorator is a function that accepts a function as argument,
and returns a function.

----
@register
def add(a, b):
    return a + b
----

Is like 

----
def add(a, b):
    return a + b

add = register(add)
----

Use cases:
- Registration

    app = FastAPI()

    @app.get('/health')
    def health_handler(): ...

    dec = app.get('/health')
    health_handler = dec(health_handler)

- Caching
"""

# %%
dispatch = {}  # name -> function


def register(fn):
    dispatch[fn.__name__] = fn
    return fn

def call(name, args):
    fn = dispatch.get(name)
    if fn is None:
        raise NameError(name)
    
    return fn(*args)

# %% caching


from functools import wraps

# IRL: use functools.lru_cache (don't use unbounded cache)
def cached(fn):
    cache =  {}

    @wraps(fn)
    def wrapper(*args, **kw):
        key = args
        if key in cache:
            print('cache hit')
            return cache[key]

        print('cache miss')
        val = cache[key] = fn(*args, **kw)
        return val

    return wrapper

# %%
@register
def add(a, b):
    print(f'add {a} {b}')
    return a + b


@register
@cached
def sub(a, b):
    print(f'sub {a} {b}')
    return a - b

# sub = register(cached(sub))

@cached
def inc(n):
    """Return n+1"""
    print(f'inc {n}')
    return n + 1

# inc = cached(inc)

call('add', (1, 7))
print(add(1, 9))

print('sub:', sub(1, 10))
print('sub:', sub(1, 10))
print('inc:', inc(7))


# Exercise: Write a retry decorator that will runs
# a function up to 3 times.
# Bonus: Raise the last exception
from random import random

def retry(fn):
    @wraps(fn)
    def wrapper(*args, **kw):
        n = 3
        e = None
        for i in range(n):
            print(f'attempt {i}')
            try:
                return fn(*args, **kw)
            except Exception as err:
                e = err
        # raise RuntimeError(f'{fn.__name__} failed {n} times')
        raise e

    return wrapper

@retry
def call_me_maybe():
    print('I just met you')
    if random() < 0.5:
        raise ValueError('This is crazy')
    print("Here's my number")


call_me_maybe()


# %%

err = None
try:
    1/0
except ZeroDivisionError as err:
    pass
    # try:
    #    pass
    # finally:
    #    del err

print(err)


# %%

def metrics(fn):
    n = 0

    @wraps(fn)
    def wrapper(*args, **kw):
        nonlocal n

        n += 1
        print(f'{fn.__name__} calls {n} times')
        return fn(*args, **kw)

    return wrapper


@metrics
def mul(a, b):
    return a * b


mul(2, 7)
mul(2, 8)


# %%
# Mutation vs re-binding
# mutation: changing existing object 
#   - cart.append('magic wand')
# re-binding: variable points to a different object
#   - cart = ['magic wand', 'invisibility cloak']
# You can re-bind only in the same scope(*)

count = 1

def inc():
    global count

    count += 1  # count = count + 1 -> re-bind

inc()
print(count)


# %% decorator with parameter
from functools import lru_cache

# Option 1: function that return decorator
# Option 2: Use a class

class lru_cache:
    def __init__(self, size):
        self.size = size

    def __call__(self, fn):
        @wraps(fn)
        def wrapper(*args, **kw):
            ...
        return wrapper



@lru_cache(1024)
def div(a, b):
    return a / b

# dec = lru_cache(1024)
# div = dec(div)













