"""Properties

Use cases:
- Calculated attributes (without breaking API)
- Setter/getter

"""

# %%
class Trade:
    def __init__(self, symbol, price, volume):
        self.symbol = symbol
        self.price = price  # calls price.setter
        self.volume = volume

    @property  # getter (t.price)
    def price(self):
        # print('get price')
        return self._price

    # price = property(_get_price, _set_price)

    @price.setter  # setter (t.price = 232.2)
    def price(self, value):
        # print('set price')
        if not isinstance(value, float):
            raise TypeError('price must be float')
        if value <= 0:
            raise ValueError(f'{value}: invalid price')
        
        self._price = value
    
    @property
    def worth(self):
        return self.price * self.volume

t1 = Trade('BRK.A', 623_250.00, 1)
print('worth:', t1.worth)
t1.volume = 2
print('worth:', t1.worth)
t1.price += 123.45
print(t1.__dict__)









