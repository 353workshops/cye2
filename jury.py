# Twelve Angry Man
#%%

# from concurrent.futures import ProcessPoolExecutor
from concurrent.futures import ThreadPoolExecutor
from time import sleep
from random import random
from threading import Lock

lock = Lock()
guilty = 0


def juror(id):
    global guilty

    sleep(random() / 1000)  # Juror thinks
    # print(f'J{id} ', end='')
    # guilty += 1  # data race
    with lock:
        guilty += 1


# with ProcessPoolExecutor() as pool:
with ThreadPoolExecutor() as pool:
    for i in range(1200):
        pool.submit(juror, i)

print(guilty)
