"""Checking sha25 signatures

Write a function that gets an index file with names of files and sha256
signatures in the following format
0c4ccc63a912bbd6d45174251415c089522e5c0e75286794ab1f86cb8e2561fd  taxi-01.csv
f427b5880e9164ec1e6cda53aa4b2d1f1e470da973e5b51748c806ea5c57cbdf  taxi-02.csv
4e251e9e98c5cb7be8b34adfcb46cc806a4ef5ec8c95ba9aac5ff81449fc630c  taxi-03.csv
...

You should compute concurrently sha256 signatures of these files and see if
they math the ones in the index file.

- Print the number of processed files
- If there's a mismatch, print the offending file(s) and exit the program with
  non-zero value

Get taxi.tar from https://storage.googleapis.com/353solutions/c/data/taxi.tar and
extract it.

$ cd /tmp
$ curl -LO https://storage.googleapis.com/353solutions/c/data/taxi.tar
$ tar xf taxi.tar
"""
import bz2
from functools import partial
from hashlib import sha256

chunk_size = 10 * 1024


def file_sha256(path):
    h = sha256()
    with bz2.open(path) as fp:
        for chunk in iter(partial(fp.read, chunk_size), b''):
            h.update(chunk)

    return h.hexdigest()


def parse_sig_file(path):
    with open(path) as fp:
        for line in fp:
            yield line.split()


def check_sig(path, expected):
    sig = file_sha256(path)
    return sig == expected


if __name__ == '__main__':
    from time import monotonic
    from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor

    root = '/tmp/taxi'
    sigs = list(parse_sig_file(f'{root}/sha256sum.txt'))
    sigs = [(sig, f'{root}/{path}.bz2') for sig, path in sigs]

    start = monotonic()

    ok = True
    args = []
    # Fan out
    with ThreadPoolExecutor() as pool:
    # with ProcessPoolExecutor() as pool:
        for sig, path in sigs:
            fut = pool.submit(check_sig, path, sig)
            # path is the call context
            args.append((fut, path))
    
    # Gather result
    for fut, path in args:
        if not fut.result():
            print(path)
            ok = False

    duration = monotonic() - start
    print(f'duration: {duration:.3f}sec {ok=}')
