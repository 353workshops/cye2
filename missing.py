# %%

from typing import Any


class DB:
    def get(self, key):
        return 7

class UserStore(dict):
    def __init__(self, db):
        self.db = db

    def __missing__(self, key):
        print(f'cache miss for {key!r}')
        val = self.db.get(key)
        self[key] = val
        return val

c = UserStore(DB())
print(c['y'])
print(c['y'])


# %%
class Player:
    count = 0
    version = '1.2.3'

    def __init__(self, name):
        # self.count += 1
        # self.count = self.count + 1
        Player.count += 1
        self.name = name


p1 = Player('Parzival')
print(Player.count)
print(p1.count)
print(p1.__dict__)

class Admin(Player):
    pass

# %% How attribute access work

def find_attr(obj, attr):
    """Emulate built-in getattr"""

    if attr in obj.__dict__:
        print(f'found {attr} in instance')
        return obj.__dict__[attr]

    cls = obj.__class__
    if attr in cls.__dict__:
        print(f'found {attr} in class')
        return cls.__dict__[attr]

    for pcls in cls.__mro__:
        if attr in pcls.__dict__:
            name = pcls.__name__
            print(f'found {attr} in {name}')
            return pcls.__dict__[attr]

    # if getattr(obj, '__getattr__'):
    #     return obj.__getattr__(attr)
    
    raise AttributeError(attr)


print('name:', find_attr(p1, 'name'))
print('version:', find_attr(p1, 'version'))

a = Admin('Art3mis')
print(find_attr(a, 'version'))


# %%

class Proxy:
    def __init__(self, obj):
        self._obj = obj

    def __getattr__(self, attr):
        print('__getattr__')
        print(f'Proxy to {attr}')
        return getattr(self._obj, attr)

    # def value(self):
    #     return 7

    # def __getattribute__(self, name):
    #     print('__getattrbute__')
    #     # return self.value()  # BUG: infinite recursion
    #     return super().__getattribute__('value)()


# __getattr__ vs __getattribute__
# __getattr__ is called when the default attribute access fails
# __getattribute__ bypasses the attribute access

p = Proxy({})
print('obj:', p._obj)
print('keys:', p.keys())


















































