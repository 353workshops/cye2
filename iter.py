"""Iterables, iterators & generators

Iterable: Concrete type (list, dict ...)
Iterator: Does the actual iteration
    Can do only two things:
    - Fetch next item (next)
    - Signal it's done (raise StopIteration)
Generator: A way to write iterators
"""

# %%
pines = ['Mabel', 'Dipper', 'Stan']  # iterable
for name in pines:
    print(name)

# %%
# What Python's "for" above does

it = iter(pines)  # Extract iterator from iterable
while True:
    try:
        value = next(it)
        print(value)
    except StopIteration:
        break
# %%

# Vickrey auction
def second_largest(iterable):
    """Return second largets item in iterable.
    Iterable must contain at least 2 items.
    """
    it = iter(iterable)
    try:
        max_val, second_val = next(it), next(it)
    except StopIteration:
        raise ValueError('itrerable with less than 2 values')

    if max_val < second_val:
        max_val, second_val = second_val, max_val  # unpacking

    for val in it:
        if max_val > val > second_val:
            second_val = val
        elif val > max_val:
            max_val, second_val = val, max_val

    return second_val

print(second_largest({1,2,3,4,5}))


# %%
for i in range(3):
    print(i)

# %%
# 2 ways to write itertors:
# - Class with __iter__ and __next__
# - generator

def irange(n):
    print('<DEBUG> start')
    i = 0
    while i < n:
        print(f'<DEBUG> before {i=}')
        yield i
        i += 1
        print(f'<DEBUG> after {i=}')
    print('<DEBUG> ciao')

for n in irange(3):
    print(n)

# ir = irange(3)
# n = next(ir)
# print('>>>', n)
# n = next(ir)
# print('>>>', n)
# n = next(ir)
# print('>>>', n)
# n = next(ir)
# print('>>>', n)

# %%

def test(db): ...

def fixture():
    print('setup')
    yield  # DB, ....
    print('teardown')

# What pytest does
fx = fixture()
db = next(fx)
try:
    test(db)
finally:
    try:
        next(fx)
    except StopIteration:
        pass

# arr[1:...:7]


# %%

def random():
    while True:
        val = yield 7
        print('val:', val)
        if val:
            return

r = random()
print(next(r))
print(next(r))
print(r.send(True))


# %%
# Exercise: Implement fgrep on a file.
# It should yield matching lines

def fgrep(file_name, expr):
    with open(file_name) as fp:
        for line in fp:
            if expr in line:
                yield line

    print('file closed')


for match in fgrep('iter.py', 'def'):
    print(match)


# %%

fg = fgrep('iter.py', 'def')
print(next(fg))
print('done')


# %% Fix fgrep leaking file descriptor
# Lesson: Acquire/free resources outside of generators


def fpgrep(fp, term):
    for line in fp:
        if term in line:
            yield line


with open('iter.py') as fp:
    fpg = fpgrep(fp, 'def')
    print(next(fpg))


# %%
from pathlib import Path

def grep_dir(dir_name, term):
    for file_name in Path('.').rglob(f'{dir_name}/**/*.py'):
        with open(file_name) as fp:
            # for line in fpgrep(fp, term):
            #     yield line
            yield from fpgrep(fp, term)

for line in grep_dir('.', '"""'):
    print(line)


# %%
# Making sure we close all files
from contextlib import closing

with closing(grep_dir('.', '"""')) as g:
    print(next(g))

# closing will call g.close() on __exit__
# g.close() will raise GeneratorExit inside grep_dir


# %%

# Streams of data: filter, map, reduce (sum, max ...)

# Exercise:
# What is the number of lines in http.log.gz where the status code
# is an error (>= 400)
# Load only one line to memory
# Use functional programming (filter, map, reduce)

# Example line:
# uplherc.upl.com - - [01/Aug/1995:00:00:07 -0400] "GET / HTTP/1.0" 304 0

# In awk:
# cat data/http.log.gz | gunzip | \
#    awk '{if ($--NF >= 400) then count++ fi} END {print count}'

# grep + wc
# cat data/http.log.gz | gunzip | \
#     grep -E '[45][0-9]+ ([0-9]+|-)$' | wc -l

import gzip

# Iterative way
n = 0
with gzip.open('data/http.log.gz', 'rt') as fp:
    for line in fp:
        fields = line.split()
        code = int(fields[-2])
        if code >= 400:
            n += 1
print(n)

# %%

def line_status(line):
    return int(line.split()[-2])


with gzip.open('data/http.log.gz', 'rt') as fp:
    statuses = map(line_status, fp)
    errors = filter(lambda n: n >= 400, statuses)
    n = sum(1 for _ in errors)  # reduce
print(n)


# %%
# List comprehension, Generator expression

# Sum of square of all odd numbers up to 10
son = [i*i for i in range(10) if i % 2 == 1]
# SQL: SELECT i*i FROM range(10) WHERE i % 2 == 1
# i*i: map
# if i % 2 == 1: filter
# sum: reduce
print(sum(son))

# %%

# son = sum((i*i for i in range(10) if i % 2 == 1))
# if generator expression is the only argument 
# you can omit the ()
# %%

son = sum(i*i for i in range(10_000_000) if i % 2 == 1)
print(son)


# %%

def line_status(line):
    return int(line.split()[-2])


with gzip.open('data/http.log.gz', 'rt') as fp:
    n = sum(1 for line in fp if line_status(line) >= 400)
print(n)

# %% iteration utilities

names = ['daffy', 'bugs', 'taz']
colors = ['black', 'gray', 'brown']

# Print names in reverse order
# for name in names[::-1]:
for name in reversed(names):
    print(name)

# Print name and index of name
for i, name in enumerate(names):
    print(f'{name} at {i}')

# Print name and color
for name, color in zip(names, colors):
    print(f'{name} is {color}')

# Tip: range(len) is a "code smell"

# for i in range(len(names)):
#     name = names[i]
#     color = colors[i]
#     print(f'{name} is {color}')


# zip will stop at the shortest sequence


# %% Quiz: What will printed?

nums = [4, 1, 3, 2]
rev = reversed(nums)
print(sorted(rev) == sorted(rev))




# %% equality vs identity

a = [1, 2, 3]
b = [1, 2, 3]
print('equal:', a == b)
print('ident:', a is b)

# %%

from itertools import count

next_id = count(1).__next__

print(next_id())
print(next_id())
print(next_id())