"""Defining and calling functions"""

# %%
def sub(a, b):
    """Returns a minus b
    
    >>> sub(1, 7)
    -6
    """
    return a - b


# %%
print(sub(1, 7))  # positional
print(sub(a=1, b=7))  # keyword
print(sub(1, b=7))
# print(sub(a=1, 7))  # syntax error

args = (1, 7)
print(sub(*args)) # print(sub(1, 7))
# * in function call will unpack a sequence
# to positional

args = {'a': 1, 'b': 7}
print(sub(**args)) # print(sub(a=1, b=7))
# ** in function call will unpack a sequence
# to keyword

# %%
def vargs(a, *args):
    print(f'{a=}')
    print(f'{args=}')  # args is a tuple

vargs(1)
vargs(1, 2, 3)
# * in function definition will pack positional to
# a tuple

# %%
def kw(a, **kw):
    print(f'{a=}')
    print(f'{kw=}')  # kw is a dict

kw(7)
kw(7, x=1, y=2)
# ** in function definition will pack keyword
# to a dict

def plot(xs, ys, color='black', size=1, opacity=1):
    ...


def get_from_db(month): ...


def monthly_report(month, **kw):
    xs, ys = get_from_db(month)
    plot(xs, ys, **kw)

# %%
def eat_all(*args, **kw):
    print(f'{args=}')
    print(f'{kw=}')

eat_all()
eat_all(1, 2)
eat_all(1, x=2)

# %%
# Everything after the * must be passed as keyword
def add(x, y, *, verbose=True):
    if verbose:
        print(f'{x} + {y}')
    return x + y

# add(1, 2, True)  # Not clear what True means
add(1, 2, verbose=True)

# %%

class Adder:
    def __init__(self, n):
        self.n = n

    def __call__(self, val):
        return self.n + val

add3 = Adder(3)
print(add3(4))


# %% Same with closure


def make_adder(n):
    def add(val):
        return n + val  # n comes from closure
    return add

add4 = make_adder(4)
print(add4(9))


# %%
type T = int|float
def mul[T](a: T, b: T) -> T:
    return a * b

print(mul(3, 4))
print(mul('3', 4))

# %%
async def connect_db():
    print()
    return 'DB'

db = connect_db()  # must await

# %%
from functools import partial

add5 = partial(add, 5)
print(add5(10))

# %% Compact whitespace in text
# 'a  b' -> 'a b'
import re

compact_ws = partial(re.compile(r'\s+').sub, ' ')

print(compact_ws('how   are\tyou'))






























































# %%
if __name__ == '__main__':
    import doctest

    doctest.testmod(verbose=True)