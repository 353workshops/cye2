# %%
a, b = 1, 2  # unpacking
print(a, b)
args = [1, 2]
a, b = args
print(a, b)

x = 1, 2  # x is a tuple
print(x)

# %%
v = (1)
print(v) # v is an int, need , in tuple
# %%

i, arr = 0, [10, 20, 30]
i, arr[i] = i + 1, 200
# i = i + 1
# arr[i] = 200
print(arr)

# Python first evaluates the right hand side
# And then assign from left to right

# %%

i, arr = 0, [10, 20, 30]
arr[i], i = 200, i + 1
# arr[i] = 200
# i = i + 1
print(arr)

# Lesson: No side effects in unpacking

# %%

def append_to(values, n):
    values += range(n)
    # values = values + range(n)  # error
    # values = values + list(range(n))  # [1]

values = [1]
append_to(values, 3)
print(values)  # [1, 0, 1, 2]

# list += is translated to append

# %% calculator UI

buttons = [lambda: print(i) for i in range(10)]
button = buttons[3]
button()  # Clicked

# All i's point to the same i which is 9 at the end
# of the list comprehension
# Known as: Closure bug

# %%
buttons = [lambda i=i: print(i) for i in range(10)]
button = buttons[3]
button()  # Clicked

# %%
def make_button(i):
    return lambda: print(i)

buttons = [make_button(i) for i in range(10)]
button = buttons[3]
button()  # Clicked

# Lesson: Understand where names come from
# Variable scope: LCGB
# Local, Closure, Global, Built-in

# %%
import builtins
builtins.color = 'green'
print(color)

# %%
# default function arguments are evaluated
# once at function definition
# def append_to(n, items=[]):
def append_to(n, items=None):
    items = [] if items is None else items
    items.append(n)
    return items

print(append_to(1))
print(append_to(2))

# Lesson: Never use mutable default values

# %%
from dataclasses import dataclass, field

@dataclass
class VM:
    id: str
    # tags: list = []  # Will raise
    tags: list = field(default_factory=list)


vm1 = VM('one')
vm2 = VM('two')
vm2.tags.append('qa')
print(vm1)

# mutable: list, dict, set
# immutable: numbers, tuple, str

# %% Aside: Numbers
n = 1
print(type(n))

f = 1.23
print(type(f))

i = 2+3j
print(type(i))

# decimal, rat, ...
π = 3.14
print(π)

print(0.1 + 0.1 + 0.1)
# Don't use float to represent money
nan = float('nan')
print(nan == nan)


# %%

# mat = [[1, 2, 3]] * 3
mat = [[1, 2, 3] for _ in range(3)]
mat[0][0] = 100
print(mat[1])








